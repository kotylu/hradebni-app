CREATE TABLE `class` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`name` varchar(128) NOT NULL UNIQUE,
	`room` int NOT NULL,
	`created` DATETIME DEFAULT CURRENT_TIMESTAMP,
	`updated` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `student` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`class_id` bigint NOT NULL,
	`name` varchar(255) NOT NULL,
	`active` bit NOT NULL DEFAULT 1,
	`created` DATETIME DEFAULT CURRENT_TIMESTAMP,
	`updated` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);
ALTER TABLE `student` ADD CONSTRAINT `student_fk0` FOREIGN KEY (`class_id`) REFERENCES `class`(`id`);



insert into class(name, room) values ('p4a', 300);
insert into student(class_id, name, active) values (1, 'Lukas Kotyza', 1);