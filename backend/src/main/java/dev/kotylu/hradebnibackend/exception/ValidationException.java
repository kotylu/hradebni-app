package dev.kotylu.hradebnibackend.exception;

public class ValidationException extends Exception {
	private String message;

	public ValidationException() {}

	public ValidationException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}
}