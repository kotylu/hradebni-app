package dev.kotylu.hradebnibackend.service;

import dev.kotylu.hradebnibackend.dao.api.StudentClassDao;
import dev.kotylu.hradebnibackend.model.StudentClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentClassService {

    @Autowired
    private final StudentClassDao classDao;

    public StudentClassService(@Qualifier("mysql-class") StudentClassDao classDao) {
        this.classDao = classDao;
    }

    public void addStudentClass(StudentClass clazz) {
        if (this.classDao.getStudentClassByName(clazz.getName()) == null) {
            this.classDao.addStudentClass(clazz);
        }
    }

    public StudentClass getStudentClass(int id) {
        return this.classDao.getStudentClass(id);
    }

    public List<StudentClass> getAllStudentClass() {
        return this.classDao.getAllStudentClass();
    }
}
