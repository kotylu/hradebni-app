package dev.kotylu.hradebnibackend.service;

import dev.kotylu.hradebnibackend.dao.api.StudentClassDao;
import dev.kotylu.hradebnibackend.dao.api.StudentDao;
import dev.kotylu.hradebnibackend.model.Student;
import dev.kotylu.hradebnibackend.model.StudentClass;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.*;

import java.util.List;

@Service
public class StudentService {
    private final StudentDao studentDao;
    private final StudentClassDao classDao;

    @Autowired
    public StudentService(@Qualifier("mysql-student") StudentDao studentDao,
                          @Qualifier("mysql-class") StudentClassDao classDao) {
        this.studentDao = studentDao;
        this.classDao = classDao;
    }

    public Student addStudent(Student student) {
        if (this.classDao.getStudentClass(student.getClassId()) != null) {
            return this.studentDao.addStudent(student);
        }
        System.err.println("class not found");
        return null;
    }

    public Student getStudent(int id) {
        return this.studentDao.getStudent(id);
    }

    public List<Student> getAllStudents() {
        return this.studentDao.getAllStudents();
    }

    public List<Student> getStudentForClass(int classId) {
        return this.studentDao.getStudentForClass(classId);
    }
}
