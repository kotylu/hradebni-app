package dev.kotylu.hradebnibackend.rest;

import dev.kotylu.hradebnibackend.model.StudentClass;
import dev.kotylu.hradebnibackend.service.StudentClassService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/class")
public class StudentClassController {

    @Autowired
    private final StudentClassService classService;

    public StudentClassController(StudentClassService classService) {
        this.classService = classService;
    }

    @PostMapping
    public void addStudentClass(@RequestBody StudentClass clazz) {
        this.classService.addStudentClass(clazz);
    }

    @GetMapping(path = "/{id}")
    public String getStudentClass(@PathVariable("id") Integer id) {
        StudentClass clazz = this.classService.getStudentClass(id);
        if (clazz != null) {
            return new JSONObject(clazz).toString();
        }
        return new JSONObject().toString();
    }

    @GetMapping
    public String getAllStudentClass() {
        return new JSONArray(this.classService.getAllStudentClass()).toString();
    }
}
