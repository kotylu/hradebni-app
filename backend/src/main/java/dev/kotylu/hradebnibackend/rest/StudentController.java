package dev.kotylu.hradebnibackend.rest;

import dev.kotylu.hradebnibackend.model.Student;
import dev.kotylu.hradebnibackend.service.StudentService;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import org.json.*;

@RestController
@RequestMapping("api/student")
public class StudentController {
    @Autowired
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public void addStudent(@RequestBody Student student) {
        this.studentService.addStudent(student);
    }

    @GetMapping(path = "/{id}")
    public String getStudent(@PathVariable("id") Integer id) {
        Student result = this.studentService.getStudent(id);
        if (result != null) {
            return new JSONObject(result).toString();
        }
        return new JSONObject().toString();
    }

    @GetMapping(path = "/class/{classId}")
    public String getStudentsForClass(@PathVariable("classId") Integer classId) {
        return new JSONArray(this.studentService.getStudentForClass(classId)).toString();
    }

    @GetMapping
    public String getAllStudents() {
        return new JSONArray(this.studentService.getAllStudents()).toString();
    }
}
