package dev.kotylu.hradebnibackend.model;

import java.util.Date;

public class StudentClass {
    private int id;
    private String name;
    private int room;
    private Date created;
    private Date updated;

    public StudentClass(int id, String name, int room, Date created, Date updated) {
        this.id = id;
        this.name = name;
        this.room = room;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getRoom() {
        return room;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
}
