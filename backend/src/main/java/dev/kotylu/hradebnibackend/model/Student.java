package dev.kotylu.hradebnibackend.model;

import java.util.Date;

public class Student {
    private int id;
    private int classId;
    private String name;
    private boolean active;
    private Date created;
    private Date updated;

    public Student(int id, int classId, String name, boolean active, Date created, Date updated) {
        this.id = id;
        this.classId = classId;
        this.name = name;
        this.active = active;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClassId() {
        return this.classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
