package dev.kotylu.hradebnibackend.dao.dbconnector.impl;

import dev.kotylu.hradebnibackend.dao.dbconnector.api.DbConnector;
import org.springframework.stereotype.Component;
import java.sql.*;

@Component("mysql-db")
public class MySqlConnector implements DbConnector {
	private final String url;
	private final String username;
	private final String passwd;

	public MySqlConnector() {
		this.url = "jdbc:mysql://db:3306/hradebni";
		this.username = "root";
		//this.passwd = System.getenv("mysql_root_passwd");
		this.passwd = "SECRET_pass";
	}

	@Override
	public Connection getConnection() {
		try {
			return DriverManager.getConnection(this.url, this.username, this.passwd);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return null;
	}
}
