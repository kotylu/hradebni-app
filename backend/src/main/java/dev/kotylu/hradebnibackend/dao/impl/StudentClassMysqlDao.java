package dev.kotylu.hradebnibackend.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import dev.kotylu.hradebnibackend.dao.api.StudentClassDao;
import dev.kotylu.hradebnibackend.dao.dbconnector.api.DbConnector;
import dev.kotylu.hradebnibackend.model.StudentClass;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.*;
import java.sql.*;

@Repository("mysql-class")
public class StudentClassMysqlDao implements StudentClassDao {

    @Autowired
    private final DbConnector db;

    public StudentClassMysqlDao(@Qualifier("mysql-db") DbConnector db) {
        this.db = db;
    }

    @Override
    public StudentClass addStudentClass(StudentClass clazz) {
        String query = "insert into class (name, room) values (?, ?);";
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, clazz.getName());
            ps.setInt(2, clazz.getRoom());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return clazz;
    }

    @Override
    public StudentClass getStudentClass(int id) {
        String query = "select * from class where id = ?;";
        StudentClass result = null;
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = makeStudentClassFromRS(rs);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    @Override
    public StudentClass getStudentClassByName(String name) {
        String query = "select * from class where name = ?;";
        StudentClass result = null;
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = makeStudentClassFromRS(rs);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    @Override
    public List<StudentClass> getAllStudentClass() {
        String query = "select * from class;";
        List<StudentClass> result = new ArrayList<>();
        try (Connection con = this.db.getConnection()) {
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                result.add(makeStudentClassFromRS(rs));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    private StudentClass makeStudentClassFromRS(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int room = rs.getInt("room");
        Date created = rs.getDate("created");
        Date updated = rs.getDate("updated");
        return new StudentClass(id, name, room, created, updated);
    }

}
