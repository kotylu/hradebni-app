package dev.kotylu.hradebnibackend.dao.api;

import dev.kotylu.hradebnibackend.model.Student;
import dev.kotylu.hradebnibackend.model.StudentClass;

import java.util.List;

public interface StudentDao {
    Student addStudent(Student student);
    Student getStudent(int id);
    List<Student> getAllStudents();
    List<Student> getStudentForClass(int classId);
    List<Student> getStudentForClass(StudentClass clazz);

}
