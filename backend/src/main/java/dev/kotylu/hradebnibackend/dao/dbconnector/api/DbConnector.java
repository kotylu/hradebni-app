package dev.kotylu.hradebnibackend.dao.dbconnector.api;

import org.springframework.stereotype.Component;
import java.sql.Connection;

@Component
public interface DbConnector {
	Connection getConnection();
}
