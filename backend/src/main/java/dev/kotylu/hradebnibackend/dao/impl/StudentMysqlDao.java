package dev.kotylu.hradebnibackend.dao.impl;

import dev.kotylu.hradebnibackend.dao.dbconnector.api.DbConnector;
import dev.kotylu.hradebnibackend.dao.api.StudentDao;
import dev.kotylu.hradebnibackend.model.StudentClass;
import org.springframework.stereotype.Repository;
import dev.kotylu.hradebnibackend.model.Student;
import org.springframework.beans.factory.annotation.*;
import java.util.Date;
import java.util.*;
import java.sql.*;

@Repository("mysql-student")
public class StudentMysqlDao implements StudentDao {

    @Autowired
    private final DbConnector db;

    public StudentMysqlDao(@Qualifier("mysql-db") DbConnector db) {
        this.db = db;
    }

    @Override
    public Student addStudent(Student student) {
        String query = "insert into student(class_id, name, active) values (?, ?, ?)";
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, student.getClassId());
            ps.setString(2, student.getName());
            ps.setInt(3, student.isActive() ? 1 : 0);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return student;
    }

    @Override
    public Student getStudent(int id) {
        String query = "select * from student where id = ?";
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return this.makeStudentFromRS(rs);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Student> getAllStudents() {
        List<Student> result = new ArrayList<>();
        String query = "select * from student";
        try (Connection con = this.db.getConnection()) {
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                result.add(this.makeStudentFromRS(rs));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Student> getStudentForClass(int classId) {
        List<Student> result = new ArrayList<>();
        String query = "select * from student where class_id = ?";
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, classId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result.add(this.makeStudentFromRS(rs));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Student> getStudentForClass(StudentClass clazz) {
        return this.getStudentForClass(clazz.getId());
    }

    private Student makeStudentFromRS(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        int classId = rs.getInt("class_id");
        String name = rs.getString("name");
        boolean active = rs.getBoolean("active");
        Date created = rs.getDate("created");
        Date updated = rs.getDate("updated");
        return new Student(id, classId, name, active, created, updated);
    }
}
