package dev.kotylu.hradebnibackend.dao.api;

import java.util.List;
import dev.kotylu.hradebnibackend.model.StudentClass;

public interface StudentClassDao {
    StudentClass addStudentClass(StudentClass clazz);
    StudentClass getStudentClass(int id);
    StudentClass getStudentClassByName(String name);
    List<StudentClass> getAllStudentClass();
}
