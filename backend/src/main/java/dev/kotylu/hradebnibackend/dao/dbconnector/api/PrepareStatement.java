package dev.kotylu.hradebnibackend.dao.dbconnector.api;

import java.sql.PreparedStatement;

public interface PrepareStatement {
	void prepare(PreparedStatement statement);
}