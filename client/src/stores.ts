import { Store } from "pullstate";
import { IDefaultValuesStore, IFormStore, ISchoolStore } from "./interfaces";

export const defaultValues : IDefaultValuesStore = {
  class: {
    id: 0,
    name: "",
    room: 0,
    created: new Date(),
    opened: false,
    createStudentFormOpened: false
  },
  student: {
    id: 0,
    classId: 0,
    name: "",
    active: true,
    created: new Date(),
    updated: new Date(),
  }
};

export const schoolStore = new Store<ISchoolStore>({
  classes: [],
  students: [],
  newClass: defaultValues.class,
  newStudent: defaultValues.student
});

export const defaultValuesStore = new Store<IDefaultValuesStore>(defaultValues);

export const formStore = new Store<IFormStore>({
  newClassOpened: false,
  newStudentOpened: false
});