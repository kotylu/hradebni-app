import React from "react";
import { IClass, ISchoolStore } from "../interfaces";
import { schoolStore } from "../stores";
import CreateStudentFormComponent from "./createStudentFormComponent";
import StudentListComponent from "./studentListComponent";

interface Props {
  clazz: IClass
}
const ClassItemComponent = ({ clazz }: Props) => {

  const classes = schoolStore.useState(x => x.classes);

  const addNewStudent = (e : React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const copy = classes.slice();
    copy.splice(classes.findIndex(c => c.id === clazz.id), 1, {
      ...clazz,
      createStudentFormOpened: !clazz.createStudentFormOpened
    });
    schoolStore.update((store : ISchoolStore) => {
      store.classes = copy;
    });
    console.log(classes);
  };

  const showStudents = (e : React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const copy = classes.slice();
    copy.splice(classes.findIndex(c => c.id === clazz.id), 1, {
      ...clazz,
      opened: !clazz.opened
    });
    schoolStore.update((store : ISchoolStore) => {
      store.classes = copy;
    });
  };

  // TODO make STUDENT FORM class-specific so we can get class_id
  // this will also prevent opening all of the forms at once
  return (
    <li key={clazz.id}>
      <div>
        <div>{clazz.name}</div>
        <div>{clazz.room}</div>
        <div>{clazz.created.toISOString().split("T")[0]}</div>
        <div>
          <button className="button" onClick={(e) => showStudents(e)}>{classes.find(c => c.id === clazz.id)?.opened ? "hide" : "show"} students</button>
          <button className="button primary-button" onClick={(e) => addNewStudent(e)}>+</button>
        </div>
      </div>
      <CreateStudentFormComponent clazz={clazz} />
      <StudentListComponent classId={clazz.id} />
    </li>
  );
};

export default ClassItemComponent;