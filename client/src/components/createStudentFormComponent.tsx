import React from "react";
import { createStudent, refreshStudents } from "../api/studentAPI";
import { IClass, ISchoolStore } from "../interfaces";
import { schoolStore } from "../stores";

interface Props {
  clazz: IClass
}
const CreateStudentFormComponent = ({ clazz } : Props) => {

  const classes = schoolStore.useState(x => x.classes);
  const rawNewStudent = schoolStore.useState(x => x.newStudent);
  const display = schoolStore.useState(x => x.classes.find(c => c.id === clazz.id)?.createStudentFormOpened);

  const newStudentNameChanged = (e : React.ChangeEvent<HTMLInputElement>) => {
    schoolStore.update((store : ISchoolStore) => {
      store.newStudent.name = e.target.value;
    });
  };

  const confirm = (e : React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    (
      async() => {
        const newStudent = {
          ...rawNewStudent,
          classId: clazz.id
        };
        await createStudent(newStudent);
        await refreshStudents();
        const copyClasses = classes.slice();
        copyClasses.splice(classes.findIndex(c => c.id === clazz.id), 1, {
          ...clazz,
          createStudentFormOpened: false,
          opened: true
        });
        schoolStore.update((store : ISchoolStore) => {
          store.classes = copyClasses;
        });
      }
    )();
  };

  return (
    <ul className={display ? "create-student-form" : "hidden"}>
      <li>
        <div>
          <label>Name</label>
          <label></label>
        </div>
        <div>
          <input onChange={(e) => newStudentNameChanged(e)} />
          <button className="button primary-button" onClick={(e) => confirm(e)}>confirm</button>
        </div>
      </li>
    </ul>
  );
};

export default CreateStudentFormComponent;