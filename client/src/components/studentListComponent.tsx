import React from "react";
import { schoolStore } from "../stores";
import StudentItemComponent from "./studentItemComponent";

interface Props {
  classId: number
}
const StudentListComponent = ({ classId } : Props) => {
  const students = schoolStore.useState(x => x.students);
  const display = schoolStore.useState(x => x.classes.find(c => c.id === classId)?.opened);

  return (
    <ul className={display ? "student-list" : "hidden"}>
      {students
        .filter(student => student.classId === classId)
        .map(student => (
          <StudentItemComponent key={student.id} student={student} />
        ))}
    </ul>
  );
};

export default StudentListComponent;