import React from "react";
import { IClass } from "../interfaces";
import { schoolStore } from "../stores";
import ClassItemComponent from "./classItemComponent";

const ClassListComponent = () => {
  const classes : IClass[] = schoolStore.useState(x => x.classes);
  return (
    <ul className="class-list">
      {
        classes.map(clazz => (
          <ClassItemComponent key={clazz.id} clazz={clazz} />
        ))
      }
    </ul>
  );
};

export default ClassListComponent;
