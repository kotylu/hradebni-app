import React from "react";
import { createClass, refreshClass } from "../api/classAPI";
import { IFormStore, ISchoolStore } from "../interfaces";
import { defaultValuesStore, formStore, schoolStore } from "../stores";

const CreateClassFormComponent = () => {
  const newClass = schoolStore.useState(x => x.newClass);
  const display = formStore.useState(x => x.newClassOpened);
  const defaultClass = defaultValuesStore.useState(x => x.class);

  const classNameChanged = (e : React.ChangeEvent<HTMLInputElement>) => {
    schoolStore.update((store : ISchoolStore) => {
      store.newClass.name = e.target.value;
    });
  };

  const classRoomChanged = (e : React.ChangeEvent<HTMLInputElement>) => {
    const rawValue = e.target.value;
    if (!/^[0-9]*$/.test(rawValue)) {
      return;
    }
    const value = Number(rawValue);
    schoolStore.update((store : ISchoolStore) => {
      store.newClass.room = value;
    });
  };
  
  const confirm = (e : React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    (
      async() => {
        await createClass(newClass);
        schoolStore.update((store : ISchoolStore) => {
          store.newClass = defaultClass;
        });
        formStore.update((store : IFormStore) => {
          store.newClassOpened = false;
        });
        await refreshClass();
      }
    )();
  };

  return (
    <form id="create-class-form" className={display ? "" : "hidden"}>
      <div>
        <label>Name</label>
        <label>Room</label>
        <label></label>
      </div>
      <div>
        <input name="class-name" value={newClass.name} onChange={(e) => {classNameChanged(e);}} />
        <input name="class-room" value={newClass.room} onChange={(e) => {classRoomChanged(e);}} />
        <button className="button button-primary" onClick={(e) => confirm(e)}>Confirm</button>
      </div>
    </form>
  );
};

export default CreateClassFormComponent;
