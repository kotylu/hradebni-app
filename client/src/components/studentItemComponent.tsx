import React from "react";
import { IStudent } from "../interfaces/studentInterface";

interface Props {
  student: IStudent
}
const StudentItemComponent = ({ student } : Props) => {
  return (
    <li>
      <div>
        <div>{student.name}</div>
        <div>{student.active ? "ACTIVE" : "DISABLED"}</div>
        <div>
          <button>disable (Not Implemented)</button>
        </div>
      </div>
    </li>
  );
};

export default StudentItemComponent;