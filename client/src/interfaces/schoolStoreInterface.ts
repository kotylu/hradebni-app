import { IClass } from "./classInterface";
import { IStudent } from "./studentInterface";

export interface ISchoolStore {
  classes: IClass[],
  students: IStudent[],
  newClass: IClass,
  newStudent: IStudent
}
