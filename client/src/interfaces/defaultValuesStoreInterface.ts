import { IClass } from "./classInterface";
import { IStudent } from "./studentInterface";

export interface IDefaultValuesStore {
  class: IClass,
  student: IStudent
}
