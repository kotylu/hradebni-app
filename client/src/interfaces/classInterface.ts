export interface IClass {
  id: number,
  name: string,
  room: number,
  created: Date,
  opened: boolean,
  createStudentFormOpened: boolean
}
