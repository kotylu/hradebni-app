export interface IStudent {
  id: number,
  classId: number,
  name: string,
  active: boolean,
  created: Date,
  updated: Date
}
