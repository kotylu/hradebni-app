import { IClass } from "./classInterface";
import { ISchoolStore } from "./schoolStoreInterface";
import { IDefaultValuesStore } from "./defaultValuesStoreInterface";
import { IFormStore } from "./formStore";

export type {
  IClass,
  ISchoolStore,
  IDefaultValuesStore,
  IFormStore
};