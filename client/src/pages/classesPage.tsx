import React from "react";
import { ClassListComponent } from "../components";
import CreateClassFormComponent from "../components/createClassFormComponent";
import { IFormStore } from "../interfaces";
import { formStore } from "../stores";

const ClassesPage = () => {

  const isFormOpen = formStore.useState(x => x.newClassOpened);

  const addNewClass = (e : React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    formStore.update((store : IFormStore) => {
      store.newClassOpened = !isFormOpen;
    });
  };

  return (
    <div>
      <ClassListComponent />
      <button className="button primary-button" onClick={(e) => addNewClass(e)}>+</button>
      <CreateClassFormComponent />
    </div>
  );
};

export default ClassesPage;