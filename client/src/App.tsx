import React, { useEffect } from "react";
import { Route, BrowserRouter as Router, Routes as Switch } from "react-router-dom";
import { refreshClass } from "./api/classAPI";
import { refreshStudents } from "./api/studentAPI";
import { ClassesPage } from "./pages";

const App = () => {

  useEffect(() => {
    (async() => {
      refreshClass();
      refreshStudents();
    })();
  }, []);

  return (
    <div>
      <Router>
        <Switch>
          <Route path="/" element={<ClassesPage />} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
