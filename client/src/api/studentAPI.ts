import { ISchoolStore } from "../interfaces";
import { IStudent } from "../interfaces/studentInterface";
import { schoolStore } from "../stores";

export const getStudents = async() => {
  const url = "/api/student";
  const response = await fetch(url);
  const data = await response.json();
  return data;
};

export const createStudent = async(data : IStudent) => {
  const url = "/api/student";
  const request = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  };
  await fetch(url, request);
};

export const refreshStudents = async() => {
  const students = await getStudents();
  if (!students) {
    return;
  }
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const typedStudents = students.map((item : any) : IStudent => ({
    id: item.id,
    classId: item.classId,
    name: item.name,
    active: item.active,
    created: new Date(item.created),
    updated: new Date(item.update)
  }));
  schoolStore.update((store : ISchoolStore) => {
    store.students = typedStudents;
  });
};