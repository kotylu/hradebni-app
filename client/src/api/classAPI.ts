import { IClass } from "../interfaces";
import { ISchoolStore } from "../interfaces";
import { defaultValues, schoolStore } from "../stores";


export const getClasses = async() => {
  const url = "/api/class";
  const response = await fetch(url);
  const data = await response.json();
  return data;
};

export const createClass = async(data: IClass) => {
  const url = "/api/class";
  const request = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  };
  await fetch(url, request);
};

export const refreshClass = async() => {
  const classes = await getClasses();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  if (!classes) {
    return;
  }
  const typedClasses = classes.map((item : any) : IClass => ({
    ...defaultValues.class,
    id: item.id,
    name: item.name,
    room: item.room,
    created: new Date(item.created),
  }));
  schoolStore.update((store : ISchoolStore) => {
    store.classes = typedClasses;
  });
};